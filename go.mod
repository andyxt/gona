module gitee.com/andyxt/gona

go 1.23.6

require (
	github.com/gofrs/uuid v4.4.0+incompatible
	github.com/gorilla/mux v1.8.1
	github.com/gorilla/websocket v1.5.3
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826
	github.com/shopspring/decimal v1.4.0
)
